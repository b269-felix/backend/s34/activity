const express = require("express");

// app - server
const app = express();

const port = 3000;

// Middlewares - software that provides common services and capabilities to applications that are not offered by the OS

// Allows your app to read JSON data 
app.use(express.json());

// allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}));

// [SECTION] Routes
// GET Method
app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!")
});

// POST Method 
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// Simple registration form

let users = [];

app.post("/signup", (request, response) => {
	if( request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send("Please input your username and password")
	};

});

// Simple change password transaction
app.patch("/change-password", (request, response) => {

	let message;

	for(let i = 0; i < users.length; i++) {
		if(request.body.username == users[i].username) {
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated!`;
			break;
		} else {
			message = "User does not exist."
		}
	};

	response.send(message);
});

app.listen(port, () => console.log(`Server running at ${port}`));


// ACTIVITY SESSION 34
// home page route with get method
app.get("/home", (request, response) => {
	response.send(`Welcome to the home page!`);
});

// home page route with get method
app.get("/home", (request, response) => {
	response.send(`Welcome to the home page!`);
});

// items page route with get method with mock database
app.get("/items", (request, response) => {
	response.send(users);
});

// delete items
app.delete("/delete-item", (request, response) => {
	if( request.body.username !== '' && request.body.password !== '') {
		users.splice(request.body);
		response.send(`User ${request.body.username} successfully deleted!`);
	}
	response.send(users);
});



